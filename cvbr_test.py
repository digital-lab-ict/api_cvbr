#!/usr/bin/python

import sys
from brpy import init_brpy

if len(sys.argv) < 3:
  print "usage: " + sys.argv[0] + " <image1> <image2>"
  exit()

print "1.init openbr..."
# br_loc is /usr/local/lib by default,
# you may change this by passing a different path to the shared objects
br = init_brpy(br_loc='/usr/local/lib')
br.br_initialize_default()
br.br_set_property('algorithm','FaceRecognition') # also made up
br.br_set_property('enrollAll','false')

print "2.loading images..."

image_query_list = list()

for i in range(2, 5):
    image = open(sys.argv[i], 'rb').read()
    image_tmpl = br.br_load_img(image, len(image))
    image_query = br.br_enroll_template(image_tmpl)
    image_query_list.append({"file": sys.argv[i], "template": image_tmpl, "query": image_query})

image = open(sys.argv[1], 'rb').read()
image_tmpl = br.br_load_img(image, len(image))
image_query = br.br_enroll_template(image_tmpl)

for i_q in image_query_list:
    scoresmat = br.br_compare_template_lists(image_query, i_q["query"])
    score = br.br_get_matrix_output_at(scoresmat, 0, 0)
    print "image: " + i_q["file"] + " score: " + str(score)


print "4.free resources..."
# clean up - no memory leaks
for i_q in image_query_list:
    br.br_free_template(i_q["template"])

br.br_free_template(image_tmpl)

print "5.dispose openbr..."

br.br_finalize()
