import io
import os
import logging
import base64
import urllib2

import cv2
from brpy import init_brpy
from PIL import Image

from flask import Flask, request, json, jsonify
from flask_cors import CORS

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
CORS(app)

DEFAULT_WIDTH=400
DEFAULT_QUALITY = 80

GALLERIES_DIR = "galleries"
GALLERY_INDEX = "index.json"
galleries = {}

@app.route("/")
def hello():
  return "api server v1.0"

@app.route("/biometrics/1.0/faces/detect", methods=['POST'])
def faces_detect():
  input = request.get_data()
  data = json.loads(input)
  ret_val = {"status": "ok", "confidence": 0.0 }
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/compare", methods=['POST'])
def faces_compare():
  logging.info("faces_compare.1")

  br = app.br
  br.br_set_property('algorithm','FaceRecognition')
  br.br_set_property('enrollAll','true')

  data = {}
  try:
    data = json.loads(request.get_data())
  except:
    logging.error("invalid input")

  logging.info("faces_compare.2.loading image 1")

  image_1 = None
  try:
    if data.get("image_1"):
      image_1 = Image.open(io.BytesIO(base64.b64decode(data.get("image_1"))))
    elif data.get("image_1_url"):
      image_1 = Image.open(urllib2.urlopen(data.get("image_1_url")))
  except Exception:
    logging.error("unable to load image 1")

  logging.info("faces_compare.3.loading image 2")

  image_2 = None
  try:
    if data.get("image_2"):
      image_2 = Image.open(io.BytesIO(base64.b64decode(data.get("image_2"))))
    elif data.get("image_2_url"):
      image_2 = Image.open(urllib2.urlopen(data.get("image_2_url")))
  except Exception:
    logging.error("unable to load image 2")

  logging.info("faces_compare.4.resizing and templating")

  image_1 = resize(image_1, DEFAULT_WIDTH)
  image_2 = resize(image_2, DEFAULT_WIDTH)

  image_1_tmpl = br.br_load_img(image_1, len(image_1))
  image_2_tmpl = br.br_load_img(image_2, len(image_2))

  logging.info("faces_compare.5.template to query")

  image_1_query = br.br_enroll_template(image_1_tmpl)
  image_2_query = br.br_enroll_template(image_2_tmpl)

  logging.info("faces_compare.6.comparing")

  score = -1.0
  if br.br_num_templates(image_1_query) > 0 and br.br_num_templates(image_2_query) > 0:
    # compare and collect scores
    scoresmat = br.br_compare_template_lists(image_1_query, image_2_query)
    score = br.br_get_matrix_output_at(scoresmat, 0, 0)

  logging.info("faces_compare.7.score: " + str(score))

  logging.info("faces_compare.8.free res")

  # clean up - no memory leaks
  br.br_free_template(image_1_tmpl)
  br.br_free_template(image_2_tmpl)
  br.br_free_template_list(image_1_query)
  br.br_free_template_list(image_2_query)

  logging.info("faces_compare.9.return")

  ret_val = {"status": "ok", "score": score}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries", methods=['POST'])
def faces_gallery_create():
  logging.info("faces_gallery_create.1")

  br = app.br
  br.br_set_property('algorithm','FaceRecognition')
  br.br_set_property('enrollAll','false')

  data = {}
  try:
    data = json.loads(request.get_data())
  except:
    logging.error("invalid input")

  gallery_name = data.get("gallery_name")
  if galleries.get(gallery_name) is not None:
    ret_val = {"status": "ko", "message": "gallery_already_exists"}
    return jsonify(ret_val)

  gallery = list()
  galleries[gallery_name] = gallery

  if not os.path.exists(GALLERIES_DIR + "/" + gallery_name):
    os.makedirs(GALLERIES_DIR + "/" + gallery_name)

  json.dump(galleries.keys(), open("galleries.json", "w"))

  ret_val = {"status": "ok"}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries", methods=['GET'])
def faces_gallery_list():
  logging.info("faces_gallery_list.1")

  ret_val = {"status": "ok", "galleries": galleries.keys()}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries/<gallery_name>", methods=['GET'])
def faces_gallery_load(gallery_name):
  logging.info("faces_gallery_load.1")

  gallery = galleries.get(gallery_name)
  if gallery is None:
    ret_val = {"status": "ko", "message": "gallery_not_found"}
    return jsonify(ret_val)

  ret_val = {"status": "ok", "gallery": {"gallery_name": gallery_name, "images": map(lambda x: x["id"], gallery)}}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries/<gallery_name>", methods=['DELETE'])
def faces_gallery_delete(gallery_name):
  logging.info("faces_gallery_delete.1")

  if galleries.get(gallery_name) is None:
    ret_val = {"status": "ko", "message": "gallery_not_found"}
    return jsonify(ret_val)

  logging.info("faces_gallery_delete.3.deleting gallery")

  del galleries[gallery_name]
  if os.path.exists(GALLERIES_DIR + "/" + gallery_name):
    os.remove(GALLERIES_DIR + "/" + gallery_name + "/" + "*.gal")
    os.rmdir(GALLERIES_DIR + "/" + gallery_name)

  ret_val = {"status": "ok"}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries/<gallery_name>/images", methods=['POST'])
def faces_enroll(gallery_name):
  logging.info("faces_enroll.1")

  br = app.br
  br.br_set_property('algorithm','FaceRecognition')
  br.br_set_property('enrollAll','false')

  data = {}
  try:
    data = json.loads(request.get_data())
  except:
    logging.error("invalid input")

  gallery = galleries.get(gallery_name)
  if gallery is None:
    ret_val = {"status": "ko", "message": "gallery_not_found"}
    return jsonify(ret_val)

  logging.info("faces_enroll.4.loading image(s)")

  images = list()
  try:
    images_data = data.get("images")
    for image in images_data:
      image_id = image.get("image_id")
      image_b64 = image.get("image_b64")
      images_url = data.get("image_url")
      image_file = None
      if image_b64:
        image_file = Image.open(io.BytesIO(base64.b64decode(image_b64)))
      elif images_url:
        image_file = Image.open(urllib2.urlopen(image_url))
      image_file = resize(image_file, DEFAULT_WIDTH)
      image_tmpl = br.br_load_img(image_file, len(image_file))
      image_query = br.br_enroll_template(image_tmpl)
      gallery.append({"id": image_id, "template": image_query})
      br_gallery_name = GALLERIES_DIR + "/" + gallery_name + "/" + image_id + ".gal"
      br_gallery = br.br_make_gallery(br_gallery_name)
      br.br_add_template_list_to_gallery(br_gallery, image_query)
      br.br_close_gallery(br_gallery)
      logging.info("faces_enroll.4.image: " + image_id + " added to gallery: " + gallery_name)
    json.dump(gallery, open(GALLERIES_DIR + "/" + gallery_name + "/" + GALLERY_INDEX, "w"))
  except Exception as e:
    logging.error("unable to load image: " + str(e))
    ret_val = {"status": "ko", "message": "invalid_image"}
    return jsonify(ret_val)

  ret_val = {"status": "ok"}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries/<gallery_name>/images/<image_id>", methods=['DELETE'])
def faces_gallery_image_delete(gallery_name, image_id):
  logging.info("faces_gallery_image_delete.1")

  data = {}
  try:
    data = json.loads(request.get_data())
  except:
    logging.error("invalid input")

  gallery = galleries.get(gallery_name)
  if not gallery:
    ret_val = {"status": "ko", "message": "gallery_not_found"}
    return jsonify(ret_val)

  for image in gallery:
    if image.get("id") == image_id:
      br.br_free_template_list(image["template"])
      gallery.remove(image)
      os.remove(GALLERIES_DIR + "/" + gallery_name + "/" + image_id + ".gal")

  ret_val = {"status": "ok"}
  return jsonify(ret_val)

@app.route("/biometrics/1.0/faces/galleries/<gallery_name>/recognize", methods=['POST'])
def faces_recog(gallery_name):
  logging.info("faces_recog.1")

  br = app.br
  br.br_set_property('algorithm','FaceRecognition')
  br.br_set_property('enrollAll','false')

  data = {}
  try:
    data = json.loads(request.get_data())
  except:
    logging.error("invalid input")
    ret_val = {"status": "ko", "message": "invalid_image"}
    return jsonify(ret_val)

  logging.info("faces_recog.2.loading image")

  gallery = galleries.get(gallery_name)
  if gallery is None:
    ret_val = {"status": "ko", "message": "gallery_not_found"}
    return jsonify(ret_val)

  image = None
  try:
    if data.get("image"):
      image = Image.open(io.BytesIO(base64.b64decode(data.get("image"))))
    elif data.get("image_url"):
      image = Image.open(urllib2.urlopen(data.get("image_url")))
    logging.info("faces_recog.4.resizing and templating")
    image = resize(image, DEFAULT_WIDTH)
  except Exception:
    logging.error("unable to load image")
    ret_val = {"status": "ko", "message": "invalid_image"}
    return jsonify(ret_val)

  image_tmpl = br.br_load_img(image, len(image))

  logging.info("faces_recog.5.template to gallery")

  image_query = br.br_enroll_template(image_tmpl)

  logging.info("faces_recog.6.comparing")

  score = -1.0
  # compare and collect scores

  results = list()
  for i_q in gallery:
    scoresmat = br.br_compare_template_lists(image_query, i_q["template"])
    score = max(br.br_get_matrix_output_at(scoresmat, 0, 0), -1.0)
    logging.info("image: " + i_q["id"] + " score: " + str(score))
    results.append({"id": i_q["id"], "score": score})

  results = sorted(results, key=lambda image: -1 * image["score"])
  logging.info("faces_recog.8.free res")

  # clean up - no memory leaks
  br.br_free_template(image_tmpl)
  br.br_free_template_list(image_query)

  logging.info("faces_recog.9.return")

  ret_val = {"status": "ok", "results": results}
  return jsonify(ret_val)

def resize(image, width, height=None):
  width_new = width if width > 0 else int(image.size[0] * (float(height) / float(image.size[1])))
  height_new = height if height > 0 else int(image.size[1] * (float(width) / float(image.size[0])))

  image = image.resize((width_new, height_new), Image.ANTIALIAS)

  image_out = io.BytesIO()
  image.save(image_out, 'JPEG', quality=DEFAULT_QUALITY)

  return image_out.getvalue()

def load_gallery(gallery_name):
  br = app.br

  gallery = json.load(open(GALLERIES_DIR + "/" + gallery_name + "/" + GALLERY_INDEX))
  if gallery is not None:
    for image in gallery:
      br_gallery = br.br_make_gallery(GALLERIES_DIR + "/" + gallery_name + "/" + image["id"] + ".gal" + "[append]")
      br_template_list = br.br_load_from_gallery(br_gallery)
      image["template"] = br_template_list
      br.br_close_gallery(br_gallery)

  return gallery

if __name__ == "__main__":
  app.br = init_brpy(br_loc='/usr/local/lib')
  app.br.br_initialize_default()

  if not os.path.exists(GALLERIES_DIR):
    os.makedirs(GALLERIES_DIR)

  logging.info("loading galleries...")

  try:
    gallery_names = json.load(open("galleries.json", "r"))
    for gallery_name in gallery_names:
      gallery = load_gallery(gallery_name)
      galleries[gallery_name] = gallery
      logging.info("gallery: " + gallery_name + " loaded")
  except IOError:
    galleries = {}

  app.run(host='0.0.0.0',port=5000,threaded=True)
